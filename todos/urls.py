from django.urls import path
from todos.views import list_view, detail_view, create_view

urlpatterns = [
    path("", list_view, name="list_view"),
    path("<int:id>/", detail_view, name="detail_view"),
    path("create/", create_view, name="create_view")
]
