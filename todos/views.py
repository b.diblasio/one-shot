from django.shortcuts import render, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

# Create your views here.
def list_view(request):
    list = TodoList.objects.all()
    context = {
        "lists": list,
    }
    return render(request, "list.html", context)

def detail_view(request, id):
    list = TodoList.objects.get(id=id)
    context = {
        "list":list,
    }
    return render(request,"detail.html", context)

def create_view(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("detail_view", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "create.html", context)
