from django.forms import ModelForm
from todos.models import TodoItem, TodoList


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = "__all__"
